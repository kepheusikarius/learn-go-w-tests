package maps

import (
	"errors"
)

// Dictionary is a map of words and their definitions
type Dictionary map[string]string

// ErrNotFound is returned from Search or Update if there's no matching key
var ErrNotFound = errors.New("not found")

// ErrDuplicateKey is returned from Add if the key already exists
var ErrDuplicateKey = errors.New("duplicate")

// Search returns the value for a given key
func (d Dictionary) Search(key string) (string, error) {
	definition, success := d[key]

	if !success {
		return "", ErrNotFound
	}

	return definition, nil
}

// Add a key without overwriting
func (d Dictionary) Add(key, value string) error {
	_, err := d.Search(key)
	if err == nil {
		return ErrDuplicateKey
	}

	d[key] = value
	return nil
}

// Update a key
func (d Dictionary) Update(key, value string) error {
	_, err := d.Search(key)
	if err != nil {
		return err
	}

	d[key] = value
	return nil
}

// Delete a key
func (d Dictionary) Delete(key string) {
	delete(d, key)
}

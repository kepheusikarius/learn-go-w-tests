package maps

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func assertDefinition(t *testing.T, d Dictionary, key string, value string) {
	got, err := d.Search(key)

	assert.NoError(t, err)
	assert.Equal(t, value, got)
}

func TestSearch(t *testing.T) {
	key := "test"
	value := "this is just a test"
	dictionary := Dictionary{key: value}

	t.Run("known word", func(t *testing.T) {
		assertDefinition(t, dictionary, key, value)
	})

	t.Run("unknown word", func(t *testing.T) {
		got, err := dictionary.Search("unknown")

		assert.Equal(t, ErrNotFound, err)
		assert.Equal(t, "", got)
	})
}

func TestAdd(t *testing.T) {
	key := "test"
	value := "this is just a test"

	t.Run("new word", func(t *testing.T) {
		dictionary := Dictionary{}
		dictionary.Add(key, value)

		assertDefinition(t, dictionary, key, value)
	})

	t.Run("existing word", func(t *testing.T) {
		dictionary := Dictionary{key: value}
		err := dictionary.Add(key, "readding same word")

		assert.Equal(t, ErrDuplicateKey, err)
	})
}

func TestUpdate(t *testing.T) {
	key := "test"
	value := "this is just a test"
	dictionary := Dictionary{key: value}

	t.Run("existing value", func(t *testing.T) {
		newValue := "new value"

		err := dictionary.Update(key, newValue)

		assertDefinition(t, dictionary, key, newValue)
		assert.NoError(t, err)
	})

	t.Run("new value", func(t *testing.T) {
		err := dictionary.Update("new key", "doesn't matter")

		assert.Equal(t, ErrNotFound, err)
	})
}

func TestDelete(t *testing.T) {
	key := "test"
	dictionary := Dictionary{key: "test definition"}

	dictionary.Delete(key)

	value, err := dictionary.Search(key)

	assert.Equal(t, "", value)
	assert.Error(t, ErrNotFound, err)
}

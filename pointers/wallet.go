package pointers

import (
	"errors"
	"fmt"
)

// ErrNonSufficientFunds is returned when a withdraw would trigger overdraft
var ErrNonSufficientFunds = errors.New("nsf")

// Currency typedef
type Currency int

// Stringifier makes a string from a thing
type Stringifier interface {
	ToString() string
}

// ToString converts a Currency to a string
func (c Currency) ToString() string {
	return fmt.Sprintf("$%d", c)
}

// Wallet of currency
type Wallet struct {
	balance Currency
}

// Deposit funds into Wallet
func (w *Wallet) Deposit(amount Currency) {
	w.balance += amount
}

// Withdraw funds from Wallet
func (w *Wallet) Withdraw(amount Currency) error {
	if amount > w.balance {
		return ErrNonSufficientFunds
	}

	w.balance -= amount
	return nil
}

// Balance of Wallet
func (w Wallet) Balance() Currency {
	return w.balance
}

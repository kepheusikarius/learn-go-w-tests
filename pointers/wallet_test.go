package pointers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDeposit(t *testing.T) {
	wallet := Wallet{}

	wallet.Deposit(10)

	assert.Equal(t, Currency(10), wallet.Balance())
}

func TestWithdraw(t *testing.T) {
	wallet := Wallet{Currency(30)}

	err := wallet.Withdraw(Currency(10))

	assert.Equal(t, Currency(20), wallet.Balance())
	assert.NoError(t, err)
}

func TestWithdrawInsufficientFunds(t *testing.T) {
	startingBalance := Currency(20)
	wallet := Wallet{startingBalance}
	err := wallet.Withdraw(Currency(100))

	assert.Error(t, err)
	assert.EqualError(t, err, ErrNonSufficientFunds.Error())
}

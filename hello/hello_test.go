package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHello(t *testing.T) {
	tests := []struct {
		name     string
		expected string
		person   string
		language string
	}{
		{"english, blank", "Hello, World", "", "en"},
		{"english, named", "Hello, Chris", "Chris", "en"},
		{"french, blank", "Salut, Monde", "", "fr"},
		{"french, named", "Salut, Denis", "Denis", "fr"},
		{"spanish, blank", "Hola, Mundo", "", "es"},
		{"spanish, named", "Hola, Pedro", "Pedro", "es"},
	}

	for _, testCase := range tests {
		t.Run(testCase.name, func(t *testing.T) {
			actual := Hello(testCase.person, testCase.language)

			assert.Equal(t, testCase.expected, actual)
		})

	}
}

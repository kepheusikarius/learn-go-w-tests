package main

import "fmt"

const englishGreeting = "Hello, "
const frenchGreeting = "Salut, "
const spanishGreeting = "Hola, "
const englishWorld = "World"
const frenchWorld = "Monde"
const spanishWorld = "Mundo"
const frenchLanguageCode = "fr"
const spanishLanguageCode = "es"

// Hello greets the named person
func Hello(name string, language string) (result string) {
	switch language {
	case spanishLanguageCode:
		result = spanishHello(name)
	case frenchLanguageCode:
		result = frenchHello(name)
	default:
		result = englishHello(name)
	}
	return
}

func englishHello(name string) string {
	if name == "" {
		name = englishWorld
	}

	return englishGreeting + name
}

func frenchHello(name string) string {
	if name == "" {
		name = frenchWorld
	}

	return frenchGreeting + name
}

func spanishHello(name string) string {
	if name == "" {
		name = spanishWorld
	}

	return spanishGreeting + name
}

func main() {
	fmt.Println(Hello("Joe", "en"))
}

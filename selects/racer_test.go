package selects

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func makeServer(sleepTime time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(sleepTime)
		w.WriteHeader(http.StatusOK)
	}))
}

func TestRacer(t *testing.T) {
	t.Run("fast vs slow", func(t *testing.T) {
		slowServer := makeServer(20 * time.Millisecond)
		fastServer := makeServer(time.Nanosecond)
		defer slowServer.Close()
		defer fastServer.Close()

		got, err := Racer(slowServer.URL, fastServer.URL)

		assert.Equal(t, fastServer.URL, got)
		assert.NoError(t, err)
	})

	t.Run("timeout", func(t *testing.T) {
		serverA := makeServer(102 * time.Millisecond)
		serverB := makeServer(101 * time.Millisecond)

		defer serverA.Close()
		defer serverB.Close()

		_, err := ConfigurableRacer(serverA.URL, serverB.URL, 100*time.Millisecond)

		assert.Error(t, err)
	})
}

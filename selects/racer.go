package selects

import (
	"errors"
	"net/http"
	"time"
)

func ping(url string) chan bool {
	ch := make(chan bool)
	go func() {
		http.Get(url)
		ch <- true
	}()
	return ch
}

var defaultTimeout = 10 * time.Second

// Racer fetches two urls and returns with the first response
func Racer(a, b string) (string, error) {
	return ConfigurableRacer(a, b, defaultTimeout)
}

// ConfigurableRacer fetches two urls and returns with the first response with a configurable timeout
func ConfigurableRacer(a, b string, timeout time.Duration) (string, error) {
	select {
	case <-ping(a):
		return a, nil
	case <-ping(b):
		return b, nil
	case <-time.After(timeout):
		return "", errors.New("timeout")
	}
}

package concurrency

// WebsiteChecker closure for testing site liveness
type WebsiteChecker func(string) bool

type wcResult struct {
	url     string
	checked bool
}

// CheckWebsitesSerial serially checks websites for liveness
func CheckWebsitesSerial(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)

	for _, url := range urls {
		results[url] = wc(url)
	}

	return results
}

// CheckWebsitesConcurrent concurrently checks websites for liveness
func CheckWebsitesConcurrent(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)
	resultChannel := make(chan wcResult)

	for _, url := range urls {
		go func(localUrl string) {
			resultChannel <- wcResult{localUrl, wc(localUrl)}
		}(url)
	}

	for i := 0; i < len(urls); i++ {
		result := <-resultChannel
		results[result.url] = result.checked
	}

	return results
}

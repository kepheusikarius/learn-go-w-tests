package iteration

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func ExampleRepeat() {
	fmt.Println(Repeat("abc", 3))
	// Output: abcabcabc
}

func TestRepeat(t *testing.T) {
	t.Run("repeat -1 times produces nothing", func(t *testing.T) {
		repeated := Repeat("a", -1)
		expected := ""

		assert.Equal(t, expected, repeated)
	})

	t.Run("repeat 5 times", func(t *testing.T) {
		repeated := Repeat("a", 5)
		expected := "aaaaa"

		assert.Equal(t, expected, repeated)
	})

	t.Run("repeat multiple characters", func(t *testing.T) {
		repeated := Repeat("abc", 3)
		expected := "abcabcabc"

		assert.Equal(t, expected, repeated)
	})
}

func Benchmark(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

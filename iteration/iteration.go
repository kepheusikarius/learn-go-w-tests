package iteration

// Repeat repeats the provided string n times
func Repeat(value string, count int) (result string) {
	for i := 0; i < count; i++ {
		result += value
	}

	return
}

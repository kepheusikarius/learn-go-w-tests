package arrays

// Sum adds all numbers in the slice
func Sum(numbers []int) (sum int) {
	for _, number := range numbers {
		sum += number
	}

	return
}

// SumAllTails adds all numbers (except the first) in each slice
func SumAllTails(slices ...[]int) (sums []int) {
	for _, s := range slices {
		if len(s) == 0 {
			sums = append(sums, 0)
		} else {
			tail := s[1:]
			sums = append(sums, Sum(tail))
		}
	}

	return
}

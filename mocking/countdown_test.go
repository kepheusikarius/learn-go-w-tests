package main

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type Spy struct {
	mock.Mock
}

const sleepCall = "sleep"
const writeCall = "write"

func (s *Spy) Sleep() {
	s.Called()
}

func (s *Spy) Write(p []byte) (n int, err error) {
	args := s.Called(p)
	return args.Int(0), args.Error(1)
}

func TestCountdown(t *testing.T) {
	t.Run("prints", func(t *testing.T) {
		spy := &Spy{}
		spy.On("Sleep").Times(4).Return()
		buffer := &bytes.Buffer{}

		Countdown(buffer, spy)

		got := buffer.String()
		want := "3\n2\n1\nGo!"

		assert.Equal(t, want, got)
	})

	t.Run("sleeps before prints", func(t *testing.T) {
		spy := &Spy{}
		spy.On("Sleep").Times(4).Return()
		spy.On("Write", mock.Anything).Times(4).Return(0, nil)

		Countdown(spy, spy)

		// assertion of call order is not possible
		// best we can currently do with testift/mock is that the expectations were met
		spy.AssertExpectations(t)
	})
}

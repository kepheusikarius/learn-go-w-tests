package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const startFrom = 3
const finalWord = "Go!"

// Sleeper makes the system sleep
type Sleeper interface {
	Sleep()
}

// Countdown counts backwards from 3, writing the result to Writer
func Countdown(w io.Writer, s Sleeper) {
	for i := startFrom; i > 0; i-- {
		s.Sleep()
		fmt.Fprintln(w, i)
	}

	s.Sleep()
	fmt.Fprint(w, finalWord)
}

type defaultSleeper struct {
	duration time.Duration
}

// Sleep sleeps for defaultSleeper.duration
func (d *defaultSleeper) Sleep() {
	time.Sleep(d.duration)
}

func main() {
	sleeper := defaultSleeper{time.Second}
	Countdown(os.Stdout, &sleeper)
}

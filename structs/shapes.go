package structs

import "math"

// Rectangle represents a four-sided figure
type Rectangle struct {
	Width, Height float64
}

// Circle shouldn't need a comment, but lint is cranky
type Circle struct {
	Radius float64
}

// Triangle shouldn't need a comment, but lint is cranky
type Triangle struct {
	Base, Height float64
}

// Shape collects operations available on geometric figures
type Shape interface {
	Area() float64
}

// Perimeter returns the perimeter of a four-sided figure
func Perimeter(rect Rectangle) float64 {
	return 2 * (rect.Width + rect.Height)
}

// Area returns the area of a Rectangle
func (rect Rectangle) Area() float64 {
	return rect.Width * rect.Height
}

// Area returns the area of a Circle
func (circle Circle) Area() float64 {
	return math.Pi * math.Pow(circle.Radius, 2)
}

// Area returns the area of a Triangle
func (triangle Triangle) Area() float64 {
	return 0.5 * triangle.Base * triangle.Height
}
